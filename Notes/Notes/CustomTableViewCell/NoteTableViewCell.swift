//
//  NoteTableViewCell.swift
//  Notes
//
//  Created by Владислав on 8/4/21.
//

import UIKit

protocol NoteTableViewCellProtocol {
    func populateNoteTableViewCell(with object: NoteCoreDataModel)
}

class NoteTableViewCell: UITableViewCell {
    @IBOutlet weak var headline: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var mainText: UILabel!
    
    
    static let reusableIdentifier = "NoteTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "NoteTableViewCell", bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
extension NoteTableViewCell: NoteTableViewCellProtocol {
    func populateNoteTableViewCell(with object: NoteCoreDataModel) {
        self.headline.text = object.headline
        self.mainText.text = object.mainText
        self.timeLabel.text = "\(object.date ?? Date())"
    }
    }
