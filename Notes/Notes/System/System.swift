//
//  System.swift
//  Notes
//
//  Created by Владислав on 8/6/21.
//

import UIKit

struct System {
    static func clearNavigationBar(forBar navigationBar: UINavigationBar) {
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
    }
}
