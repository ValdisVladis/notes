//
//  ScreenSizeSupport.swift
//  Notes
//
//  Created by Владислав on 8/6/21.
//

import UIKit

extension UIScreen {
    enum DeviceSizeType: CGFloat {
        case iPhone7Plus = 2208
        case iPhone11Pro = 2426
        case iPhone12Pro = 2532
        case iPhone12ProMax = 2778
        case Unknown = 0.0
    }
    
    var deviceSizeType: DeviceSizeType {
        let deviceHeight = self.nativeBounds.height
        guard let sizeType = DeviceSizeType(rawValue: deviceHeight) else {return .Unknown}
        return sizeType
    }
}
