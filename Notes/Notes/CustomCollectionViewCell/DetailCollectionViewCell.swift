//
//  DetailCollectionViewCell.swift
//  Notes
//
//  Created by Владислав on 8/8/21.
//

import UIKit

protocol DetailCollectionViewCellProtocol {
    func populateDetailCollectionViewCell(with object: NoteCoreDataModel)
}

class DetailCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var headlineLabel: UILabel!
    @IBOutlet weak var mainTextLabel: UITextView!
    static let reusableIdentifier = "DetailCollectionViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "DetailCollectionViewCell", bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth = 1
        self.layer.borderColor = #colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1)
        
        
    }
}
extension DetailCollectionViewCell: DetailCollectionViewCellProtocol {
    func populateDetailCollectionViewCell(with object: NoteCoreDataModel) {
        self.headlineLabel.text = object.headline
//        self.mainTextLabel.text = object.mainText
    }
    
    
}
