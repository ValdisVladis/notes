//
//  ViewController.swift
//  Notes
//
//  Created by Владислав on 8/5/21.
//

import UIKit

class DetailedViewController: UIViewController {
    //MARK: - UI elements
    @IBOutlet weak var detailCollectionView: UICollectionView!
    
    // MARK: - ViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        Persistance.shared.getAllItems()
        configureDetailCollectionView()
        self.title = "Your Notes Gallery"
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveNoteInfo(_:)), name: Notification.Name("note"), object: nil)
    }
    
    @objc func didReceiveNoteInfo(_ notification: Notification) {
        guard let noteInfo = notification.object as? String? else {return}
        Persistance.shared.createNewItem(headline: noteInfo, mainText: nil, date: nil)
        DispatchQueue.main.async {
            self.detailCollectionView.reloadData()
        }
        
    }
    
    func configureDetailCollectionView() {
        self.detailCollectionView.delegate = self
        self.detailCollectionView.dataSource = self
        self.detailCollectionView.layer.borderWidth = 5
        self.detailCollectionView.layer.borderColor = #colorLiteral(red: 1, green: 0.4536167979, blue: 0.3168803453, alpha: 1)
        self.detailCollectionView.register(DetailCollectionViewCell.nib(), forCellWithReuseIdentifier: DetailCollectionViewCell.reusableIdentifier)
    }
    
}
// MARK: - UICollectionView Extension
extension DetailedViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, contextMenuConfigurationForItemAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        let contextMenu = UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { (action) -> UIMenu? in
            let edit = UIAction(title: "Edit", image: UIImage(systemName: "square.and.pencil"), identifier: nil, discoverabilityTitle: nil, state: .off) { (_) in
            }
            let delete = UIAction(title: "Delete", image: UIImage(systemName: "trash"), identifier: nil, discoverabilityTitle: nil,attributes: .destructive, state: .off) { (_) in
                print("delete button clicked")
                Persistance.shared.deleteItem(item: Persistance.shared.notes[indexPath.item])
                self.detailCollectionView.reloadData()
            }
            return UIMenu(title: "Options", image: nil, identifier: nil, options: UIMenu.Options.displayInline, children: [edit,delete])
        }
        return contextMenu
    }
}

extension DetailedViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Persistance.shared.notes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let detailCell = collectionView.dequeueReusableCell(withReuseIdentifier: DetailCollectionViewCell.reusableIdentifier, for: indexPath) as? DetailCollectionViewCell else {return UICollectionViewCell()}
        let noteModel = Persistance.shared.notes[indexPath.item]
        detailCell.populateDetailCollectionViewCell(with: noteModel)
        return detailCell
    }
    
    
    
}
extension DetailedViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    
}
