//
//  ViewController.swift
//  Notes
//
//  Created by Владислав on 8/4/21.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    //    MARK: - UI
    @IBOutlet weak var notesTableView: UITableView!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewHeader: UIImageView!
    @IBOutlet weak var notesCount: UILabel!
    
    //    MARK: - Variables

    //    MARK: - Constants

    //    MARK: - ViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = #colorLiteral(red: 0.9699861407, green: 0.960067451, blue: 0.9776807427, alpha: 1)
        self.title = "Notes"
        Persistance.shared.getAllItems()
        configureNotesTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let navigationController = navigationController {
            System.clearNavigationBar(forBar: navigationController.navigationBar)
            navigationController.view.backgroundColor = .clear
        }
        if UIDevice().userInterfaceIdiom == .phone {
            self.headerHeightConstraint.constant = getHeaderImageHeightForCurrentDevice()
        }
        if UIScreen.main.deviceSizeType == .iPhone12ProMax {
            self.viewHeader.frame = CGRect(x: 0, y: 0, width: view.bounds.size.height, height: 175)
        }
    }

    func getHeaderImageHeightForCurrentDevice() -> CGFloat {
        switch UIScreen.main.nativeBounds.height {
        case 2426:
            return 175
        case 2778:
            return 175
        case 1920, 2208:
           return 145
        case 2532:
            return 175
        default:
            return 145
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }

    @IBAction func didTapSortButton(_ sender: UIBarButtonItem) {
        if notesTableView.isEditing {
            notesTableView.isEditing = false
        }else{
            notesTableView.isEditing = true
        }
    }
    
    @IBAction func popToDetailViewController(_ sender: UIBarButtonItem) {
        guard let detailViewController = self.storyboard?.instantiateViewController(identifier: "DetailedViewController") as? DetailedViewController else {return}
        navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    @IBAction func popToInsertViewController(_ sender: UIBarButtonItem) {
        guard let insertViewController = self.storyboard?.instantiateViewController(identifier: "InsertViewController") as? InsertViewController else {return}
        insertViewController.modalTransitionStyle = .flipHorizontal
        insertViewController.completion = { headline, date in
            self.navigationController?.popToRootViewController(animated: true)
            Persistance.shared.createNewItem(headline: headline, mainText: date, date: nil)
            self.notesCount.text = "\(Persistance.shared.notes.count) Notes"
            self.notesTableView.isHidden = false
            DispatchQueue.main.async {
                self.notesTableView.reloadData()
            }
        }
        self.navigationController?.pushViewController(insertViewController, animated: true)
    }
    
    private func configureNotesTableView() {
        notesTableView.layer.borderWidth = 1
        notesTableView.layer.borderColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        notesTableView.layer.cornerRadius = 10
        notesTableView.register(NoteTableViewCell.nib(), forCellReuseIdentifier: NoteTableViewCell.reusableIdentifier)
        notesTableView.delegate = self
        notesTableView.dataSource = self
    }
    
}
extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        Persistance.shared.notes.swapAt(sourceIndexPath.row, destinationIndexPath.row)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            Persistance.shared.deleteItem(item: Persistance.shared.notes[indexPath.row])
            tableView.reloadData()
        }
    }
    
}
extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Persistance.shared.notes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let noteCell = tableView.dequeueReusableCell(withIdentifier: NoteTableViewCell.reusableIdentifier, for: indexPath) as? NoteTableViewCell else {return UITableViewCell()}
        let noteObject = Persistance.shared.notes[indexPath.row]
        noteCell.populateNoteTableViewCell(with: noteObject)
        return noteCell
    }
}
