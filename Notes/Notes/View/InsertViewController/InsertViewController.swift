//
//  InsertViewController.swift
//  Notes
//
//  Created by Владислав on 8/4/21.
//

import UIKit

class InsertViewController: UIViewController{
    // MARK: - UI
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var dateAndTimeLabel: UILabel!
    @IBOutlet weak var labelTopAnchor: NSLayoutConstraint!
    // MARK: - Constants
    
    // MARK: - Variables
    public var completion: ((String, String) -> Void)?
    public var mainTextCompletion: ((String) ->Void)?
    var headline = ""
    var mainText = ""
    var previousRect = CGRect.zero
    // MARK: - ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(didTapDoneButton))
        textView.delegate = self
        labelTopAnchor.constant = -50
        dateAndTimeLabel.isHidden = true
        dateAndTimeLabel.text = Date.getCurrentDateAndTime()
        if let navigationController = navigationController {
            System.clearNavigationBar(forBar: navigationController.navigationBar)
            navigationController.navigationBar.tintColor = #colorLiteral(red: 1, green: 0.4536167979, blue: 0.3168803453, alpha: 1)
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //        self.textView.resignFirstResponder()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(textViewDidChangeWithNotification(_:)), name: UITextView.textDidChangeNotification, object: nil)
        
        
    }
    @objc func didTapDoneButton() {
        if let text = textView.text, !text.isEmpty{
            let date = Date.getCurrentTime()
            completion?(text, date)
            NotificationCenter.default.post(name: Notification.Name("note"), object: self.textView.text)
            
        }
        
    }
    
    @objc func textViewDidChangeWithNotification(_ notification: Notification) {
        //        if self.textView.text.last == "\n" || self.textView. {
        //
        //        }
    }
    
}
extension InsertViewController: UITextFieldDelegate {
    
}

extension InsertViewController: UITextViewDelegate, NSTextStorageDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let currentLine = textView.text ?? ""
        let attributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 30, weight: .heavy)]
        let attributedText = NSMutableAttributedString(string: currentLine, attributes: attributes)
        textView.attributedText = attributedText
        
        let newLine = textView.text ?? ""
        if newLine.last?.isNewline == true  {
            print("new line")
        }
        
    }
    
    
    
    //        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    //            let currentText = textView.text ?? ""
    //            let attributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 50, weight: .heavy)]
    //            textView.textStorage.delegate = self
    //
    //            let attributedString = NSMutableAttributedString(string: currentText, attributes: attributes)
    //            textView.attributedText = attributedString
    //
    //            if textView.text.last?.isNewline == true {
    //                print("new line")
    //                textView.textStorage.invalidateAttributes(in: range)
    //                let newLine = textView.text ?? ""
    //                let attributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: .regular)]
    //                let attributedString = NSMutableAttributedString(string: newLine, attributes: attributes)
    //                if newLine.first?.isLetter == true || newLine.first?.isNumber == true {
    //                   print("first ")
    //                textView.attributedText = attributedString
    //                textView.textStorage.addAttributes(attributes, range: range)
    //                    print(newLine)
    //                }
    //            }
    //            return true
    //
    //
    //}
}

// MARK: - ViewController extension
extension InsertViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollViewCurrentPosition = scrollView.contentOffset.y
        var dateAndTimeLabelCurrentPosition = dateAndTimeLabel.frame.origin.y
        if scrollView.isDragging == true {
            self.dateAndTimeLabel.isHidden = false
            dateAndTimeLabelCurrentPosition = scrollViewCurrentPosition
            
        }
    }
}
extension UIViewController{
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
