//
//  NoteModel.swift
//  Notes
//
//  Created by Владислав on 8/4/21.
//

import Foundation

struct Note {
    let headline: String?
    let mainText: String?
    let date: Date?
}
