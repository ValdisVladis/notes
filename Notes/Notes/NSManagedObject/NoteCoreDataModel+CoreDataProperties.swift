//
//  NoteCoreDataModel+CoreDataProperties.swift
//  Notes
//
//  Created by Владислав on 8/4/21.
//
//

import Foundation
import CoreData


extension NoteCoreDataModel {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<NoteCoreDataModel> {
        return NSFetchRequest<NoteCoreDataModel>(entityName: "NoteCoreDataModel")
    }
    
    @NSManaged public var headline: String?
    @NSManaged public var mainText: String?
    @NSManaged public var date: Date?
    
}

extension NoteCoreDataModel : Identifiable {
    
}
