//
//  Persistance.swift
//  Notes
//
//  Created by Владислав on 8/4/21.
//
import CoreData
import Foundation


class Persistance {
    static let shared = Persistance()
    private init() {}
    
    
    static var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "NoteCoreDataModel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    public var notes = [NoteCoreDataModel]()
    
    static var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    func saveContext () {
        let context = Persistance.persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func getAllItems() {
        do {
            let context = Persistance.persistentContainer.viewContext
            self.notes =  try context.fetch(NoteCoreDataModel.fetchRequest())
            
        }catch let error {
            print(error.localizedDescription)
        }
    }
    
    func createNewItem(headline: String?, mainText: String?, date: Date?) {
        let context = Persistance.persistentContainer.viewContext
        let itemToCreate = NoteCoreDataModel(context: context)
        itemToCreate.headline = headline
        itemToCreate.mainText = mainText
        itemToCreate.date = date
        
        
        do {
            try context.save()
            getAllItems()
        }catch let error{
            print("Error: \(error)")
        }
    }
    
    func deleteItem(item: NoteCoreDataModel) {
        let context = Persistance.persistentContainer.viewContext
        context.delete(item)
        do {
            try context.save()
            getAllItems()
        }catch let error{
            print("Error: \(error)")
        }
        
    }
    
    func updateObject(object: NoteCoreDataModel, newHeadline: String?, newMainText: String?, newDate: Date?) {
        let context = Persistance.persistentContainer.viewContext
        object.headline = newHeadline
        object.mainText = newMainText
        object.date = newDate
        do {
            try context.save()
            getAllItems()
        }catch let error{
            print("Error: \(error)")
        }
    }
    
}
