//
//  GlobalFunctions .swift
//  Notes
//
//  Created by Владислав on 8/4/21.
//

import UIKit
extension Date {
    static func getCurrentDateAndTime() -> String {
        let date = Date()
        let calendar = Calendar.current
        let day = calendar.component(.day, from: date)
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        return "\(day) \(month) \(year) at \(hour):\(minutes)"
    }
    
    static func getCurrentTime() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm a"
        let result = formatter.string(from: date)
        return result
    }
}
